Source: gkl
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Olivier Sallou <osallou@debian.org>,
           Pierre Gruet <pgt@debian.org>
Section: java
Priority: optional
Build-Depends: default-jdk,
               debhelper-compat (= 13),
               javahelper,
               gradle-debian-helper,
               maven-repo-helper,
               yasm,
               zlib1g-dev,
               testng,
               default-jdk-doc,
               libcommons-io-java,
               liblog4j2-java,
               libhtsjdk-java,
               libgatk-native-bindings-java,
               cmake
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/java-team/gkl
Vcs-Git: https://salsa.debian.org/java-team/gkl.git
Homepage: https://github.com/Intel-HLS/GKL
Rules-Requires-Root: no

Package: libgkl-java
Architecture: all
Depends: ${misc:Depends},
         ${maven:Depends},
         ${java:Depends},
         libgatk-native-bindings-java
Recommends: ${java:Recommends},
            libgkl-jni
Multi-Arch: foreign
Description: Java library to manipulate SAM and BAM files
 Java library with optimized versions of compute kernels used in genomics
 applications like GATK and HTSJDK.
 These kernels are optimized to run on Intel Architecture
 (AVX, AVX2, AVX-512, multicore, and FPGA) under 64-bit Linux.

Package: libgkl-jni
Architecture: amd64
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: native libraries of Java library to manipulate SAM and BAM files
 Java library with optimized versions of compute kernels used in genomics
 applications like GATK and HTSJDK.
 These kernels are optimized to run on Intel Architecture
 (AVX, AVX2, AVX-512, multicore, and FPGA) under 64-bit Linux.
 .
 This package contains the x86 native libraries used by libglk-java
